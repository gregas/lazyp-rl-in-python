# -*- coding: utf-8 -*-
"""
Created on Mon Dec 28 12:16:01 2020

@author: gregas
"""

import numpy as np


class Bandit():
    """
    One armed bandit
    """
    
    def __init__(self, prob_win, seed=None, pdf='uniform'):
        self.prob_win = prob_win
        self.iter = 0.
        self.rng = np.random.default_rng(seed=seed)
        if pdf == 'uniform':
            self.sample_pdf = self.rng.random 
        else:
            raise ValueError(f'{pdf} PDF is not implemented')
        return
    
    def pull(self):
        """
        Draws from distribution, 
        win or lose according to self.prob_win
        """
        result = 1 if self.sample_pdf(1)[0] < self.prob_win else 0
        self.update(result)
        return result, self.est_prob_win
     
    def update(self, result):
        """
        Update estimated probability of win
        """
        if self.iter == 0:
            self.est_prob_win = result
        else:
            self.est_prob_win = \
                ((self.iter-1)*self.est_prob_win + result) / self.iter
        self.iter += 1.
    

if __name__ == '__main__':
    # setup
    seed = None
    p_bandits = [.45, .5, .55, .22, .88] # prob of win for each bandits
    max_iterations = 100000
    bandits = [ Bandit(p, seed=seed) for p in p_bandits ]
    est_p_bandits = np.zeros(len(p_bandits))
    wins = 0.
  
    # eps is an array of epsilon values len(eps) = max_interations
    eps = np.zeros((max_iterations)) + 0.01 # fixed epsilon
    rng = np.random.default_rng(seed=seed)
    
    for t in range(max_iterations):
        if rng.random() < eps[t]:
            nb = rng.integers(0, len(p_bandits))
        else:
            nb = est_p_bandits.argmax()
            
        r, est_p = bandits[nb].pull()
        est_p_bandits[nb] = est_p
        wins += r
        p_win = wins / (t+1.)
        
    print(est_p_bandits)
    print(p_win)

        
       
          